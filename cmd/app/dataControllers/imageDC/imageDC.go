package imageDC

import (
	"bytes"
	"image"
	"image/jpeg"
	"image/png"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

//GetImage ...
func GetImage(w http.ResponseWriter, r *http.Request) {

	//TODO: test on remote
	wd, err := os.Getwd()
	if err != nil {
		//Can't get working directory
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	//TODO: test large image set to see if all load for effiecnecy
	params := mux.Vars(r) //get Params
	//fmt.Println(params["folder"])
	//fmt.Println(params["fileName"])

	path := params["folder"] + "/"
	if params["folder2"] != "" {
		path += params["folder2"] + "/"
	}

	path = path + params["fileName"]
	path = wd + "/cmd/app/assets/" + path
	//fmt.Println(path)
	img, contentType, err := getImageFromFilePath(path)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	switch contentType {
	case "image/jpeg":
		writeJPGImage(w, &img)
		break
	case "image/png":
		writePNGImage(w, &img)
		break
	default:
		w.WriteHeader(http.StatusInternalServerError)
		break
	}

}

func getImageFromFilePath(filePath string) (image.Image, string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, "", err
	}

	defer f.Close()

	image, _, err := image.Decode(f)
	if err != nil {
		return nil, "", err
	}
	f.Seek(0, 0)

	contentType, err := getFileContentType(f)
	if err != nil {
		//Can't get content type
		return nil, "", err
	}

	return image, contentType, err
}

func getFileContentType(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function. Always returns a valid
	// content-type by returning "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

// writeImage encodes an image 'img' in jpeg format and writes it into ResponseWriter.
func writeJPGImage(w http.ResponseWriter, img *image.Image) {

	buffer := new(bytes.Buffer)
	if err := jpeg.Encode(buffer, *img, nil); err != nil {
		//Can't encode image file
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		//Can't write image file
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}
}

func writePNGImage(w http.ResponseWriter, img *image.Image) {

	buffer := new(bytes.Buffer)
	if err := png.Encode(buffer, *img); err != nil {
		//Can't encode image file
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		//Can't write image file
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}
}
