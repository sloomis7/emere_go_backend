package userDC

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sloomis/emere_go_backend/cmd/app/models/user"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/appdata"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

//SigningKey ...
var SigningKey = []byte("emere_signing_key")

//LoginResponse ...
type LoginResponse struct {
	Token string            `json:"token"`
	User  user.ResponseUser `json:"user"`
}

//SignInFailResponse ...
type SignInFailResponse struct {
	Message string `json:"message"`
}

//LoginWithTokenResponse ...
type LoginWithTokenResponse struct {
	IsAuthorized bool              `json:"isAuthorized"`
	User         user.ResponseUser `json:"user"`
}

//Session ...
type Session struct {
	SessionToken string `json:"sessionToken"`
	UserID       string `json:"userID"`
}

//AuthFailResponse ...
type AuthFailResponse struct {
	IsAuthorized bool `json:"isAuthorized"`
}

//SignUp - sign up a New User
func SignUp(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	newUser := CreateUser(w, r)

	if newUser == nil {
		//User not Created
		return
	}
	Login(w, r, *newUser)

}

//SignIn - sign in an existing User
func SignIn(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	//TODO: signIn securtiy

	foundUser := GetUser(w, r)
	if foundUser == nil {
		//User not found or incorrect creds
		return
	}

	//Find all sessions for user ID and remove
	stringObjectID := foundUser.ID.Hex()
	RemoveAllSessions(stringObjectID)

	Login(w, r, *foundUser)

}

//LoginWithToken ...
func LoginWithToken(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	token := r.Header["Token"][0]
	client := dbclientmanager.GetDBClient()
	sessionCollection := client.Database("EmereDB").Collection("sessions")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	filter := bson.D{{"sessiontoken", token}}

	var session Session
	err := sessionCollection.FindOne(ctx, filter).Decode(&session)
	loginFailResponse := AuthFailResponse{IsAuthorized: false}

	if err != nil {
		//Seesion not found
		fmt.Println(err.Error())
		json.NewEncoder(w).Encode(loginFailResponse)
		return
	}

	userCollection := client.Database("EmereDB").Collection("users")

	objectID, err := primitive.ObjectIDFromHex(session.UserID)
	if err != nil {
		//Cannot create object ID from session user ID
		_, err = sessionCollection.DeleteOne(ctx, bson.M{"sessiontoken": token})
		if err != nil {
			//Couldn't Delete Session
			fmt.Println(err.Error())
		}
		fmt.Println(err.Error())
		json.NewEncoder(w).Encode(loginFailResponse)
		return
	}

	filter = bson.D{{"_id", objectID}}

	var userObj user.User
	err = userCollection.FindOne(ctx, filter).Decode(&userObj)

	if err != nil {
		//User not found for session
		_, err = sessionCollection.DeleteOne(ctx, bson.M{"sessiontoken": token})
		if err != nil {
			//Couldn't Delete Session
			fmt.Println(err.Error())
		}
		fmt.Println(err.Error())
		json.NewEncoder(w).Encode(loginFailResponse)
		return
	}
	stringObjectID := userObj.ID.Hex()
	responseUser := user.ResponseUser{ID: stringObjectID, FullName: userObj.FullName, PhoneNumber: userObj.PhoneNumber, Email: userObj.Email}
	loginResponse := LoginWithTokenResponse{IsAuthorized: true, User: responseUser}

	json.NewEncoder(w).Encode(loginResponse)

}

//Login ...
func Login(w http.ResponseWriter, r *http.Request, userObj user.User) {

	token, error := GenerateJWT(userObj)

	if error != nil {
		//Can't create Token
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + error.Error() + `"}`))
		return
	}

	client := dbclientmanager.GetDBClient()
	sessionCollection := client.Database("EmereDB").Collection("sessions")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	stringObjectID := userObj.ID.Hex()
	session := Session{SessionToken: token, UserID: stringObjectID}

	//TODO: add as user object relational for single find
	_, error = sessionCollection.InsertOne(ctx, session)

	if error != nil {
		//Can't Add session to DB
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + error.Error() + `"}`))
		return
	}

	responseUser := user.ResponseUser{ID: stringObjectID, FullName: userObj.FullName, PhoneNumber: userObj.PhoneNumber, Email: userObj.Email}
	loginResponse := LoginResponse{Token: token, User: responseUser}

	json.NewEncoder(w).Encode(loginResponse)
}

//GenerateJWT ...
func GenerateJWT(user user.User) (string, error) {
	token := jwt.New((jwt.SigningMethodHS256))

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["userID"] = user.ID
	//TODO:test short expire
	expirationTime := time.Duration(appdata.Shared().TokenExpirationTime)
	claims["exp"] = time.Now().Add(time.Minute * expirationTime).Unix()

	tokenString, err := token.SignedString(SigningKey)

	if err != nil {
		//Can't create Token
		return "", err
	}

	return tokenString, nil
}

//CreateUser - create a New User
func CreateUser(w http.ResponseWriter, r *http.Request) *user.User {

	client := dbclientmanager.GetDBClient()
	userCollection := client.Database("EmereDB").Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	decoder := json.NewDecoder(r.Body)

	var newUser user.User
	err := decoder.Decode(&newUser)
	if err != nil {
		//Can't decode from request structure
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return nil
	}

	// Salt and hash the password using the bcrypt algorithm
	// The second argument is the cost of hashing, which we arbitrarily set as 8 (this value can be more or less, depending on the computing power you wish to utilize)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(newUser.Password), 8)
	if err != nil {
		//Can't generate hashed password
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return nil
	}

	newUser.Password = string(hashedPassword)

	filter := bson.D{{"email", newUser.Email}}

	var foundUser user.User
	_ = userCollection.FindOne(ctx, filter).Decode(&foundUser)

	signInFailResponse := SignInFailResponse{}

	if foundUser != (user.User{}) {
		//User Already exists

		signInFailResponse.Message = "User exists already."
		json.NewEncoder(w).Encode(signInFailResponse)
		return nil
	}

	newUser.ID = primitive.NewObjectID()

	_, err = userCollection.InsertOne(ctx, newUser)

	if err != nil {
		//Can't add user to DB
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return nil
	}

	return &newUser
}

//GetUser - get user, validate password
func GetUser(w http.ResponseWriter, r *http.Request) *user.User {

	client := dbclientmanager.GetDBClient()
	userCollection := client.Database("EmereDB").Collection("users")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	decoder := json.NewDecoder(r.Body)

	var searchUser user.SignInUser
	err := decoder.Decode(&searchUser)
	if err != nil {
		//Can't decode from request structure
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return nil
	}

	filter := bson.D{{"email", searchUser.Email}}

	var foundUser user.User
	err = userCollection.FindOne(ctx, filter).Decode(&foundUser)

	signInFailResponse := SignInFailResponse{}

	if err != nil {
		//Couldn't find user for given email
		signInFailResponse.Message = "Couldn't find user for given email"
		json.NewEncoder(w).Encode(signInFailResponse)
		return nil
	}

	byteHash := []byte(foundUser.Password)

	err = bcrypt.CompareHashAndPassword(byteHash, []byte(searchUser.Password))
	if err != nil {
		//Given password doesn't match
		signInFailResponse.Message = "Given password doesn't match"
		json.NewEncoder(w).Encode(signInFailResponse)
		return nil
	}

	return &foundUser
}

//RemoveAllSessions ...
func RemoveAllSessions(userID string) {

	client := dbclientmanager.GetDBClient()
	sessionCollection := client.Database("EmereDB").Collection("sessions")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	_, _ = sessionCollection.DeleteMany(ctx, bson.M{"userid": userID})
}
