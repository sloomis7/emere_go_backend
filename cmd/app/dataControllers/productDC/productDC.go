package productDC

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sloomis/emere_go_backend/cmd/app/models/product"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//GetProductDetailsResponse ...
type GetProductDetailsResponse struct {
	IsAuthorized bool            `json:"isAuthorized"`
	Product      product.Product `json:"product"`
}

//GetProductDetails ...
func GetProductDetails(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) //get Params

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("products")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	objectID, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		//Cannot create object ID from product ID
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	filter := bson.D{{"_id", objectID}}

	// create a value into which the result can be decoded
	var foundProduct product.Product
	err = collection.FindOne(ctx, filter).Decode(&foundProduct)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	getProductResponse := GetProductDetailsResponse{IsAuthorized: true, Product: foundProduct}

	//time.Sleep(1 * time.Second)

	json.NewEncoder(w).Encode(getProductResponse)
}

//CreateProductsTest - create a Mock Products Data
func CreateProductsTest() {

	client := dbclientmanager.GetDBClient()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	names, err := client.Database("EmereDB").ListCollectionNames(ctx, bson.M{})
	if err != nil {
		// Handle error
		log.Printf("Failed to get coll names: %v", err)
		return
	}

	// Simply search in the names slice, e.g.
	found := false
	for _, name := range names {
		if name == "categories" || name == "products" {
			//log.Printf("The collection exists!")
			found = true
			break
		}
	}

	if found {
		return
	}

	productsCollection := client.Database("EmereDB").Collection("products")

	categoriesArray, productsArray := product.CreateProductsData()

	var productSlice []interface{}
	for _, t := range productsArray {
		productSlice = append(productSlice, t)
	}

	_, err = productsCollection.InsertMany(ctx, productSlice)

	if err != nil {
		//Can't add to DB
		fmt.Println(err)
		fmt.Println("failed")
		return
	}

	categoriesCollection := client.Database("EmereDB").Collection("categories")

	var categpriesSlice []interface{}
	for _, t := range categoriesArray {
		categpriesSlice = append(categpriesSlice, t)
	}

	_, err = categoriesCollection.InsertMany(ctx, categpriesSlice)

	if err != nil {
		//Can't add to DB
		fmt.Println(err)
		fmt.Println("failed")
		return
	}
	fmt.Println("Added Mock Data")
}

//RemoveProductsAndCategories ...
func RemoveProductsAndCategories() {

	client := dbclientmanager.GetDBClient()
	productsCollection := client.Database("EmereDB").Collection("products")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	_, err := productsCollection.DeleteMany(ctx, bson.M{})

	if err != nil {
		//Can't add to DB
		fmt.Println(err)
		fmt.Println("failed")
		return
	}

	categoriesCollection := client.Database("EmereDB").Collection("categories")

	_, err = categoriesCollection.DeleteMany(ctx, bson.M{})

	if err != nil {
		//Can't add to DB
		fmt.Println(err)
		fmt.Println("failed")
		return
	}
	fmt.Println("Deleted all Mock data")
}
