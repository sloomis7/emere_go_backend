package categoryDC

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/sloomis/emere_go_backend/cmd/app/models/category"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"go.mongodb.org/mongo-driver/bson"
)

//GetCategoriesResponse ...
type GetCategoriesResponse struct {
	IsAuthorized bool                `json:"isAuthorized"`
	Categories   []category.Category `json:"categories"`
}

//GetCategories ...
func GetCategories(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("categories")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	cursor, err := collection.Find(ctx, bson.M{})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	var categories = make([]category.Category, 0)

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var foundCategory category.Category
		cursor.Decode(&foundCategory)
		categories = append(categories, foundCategory)
	}

	if err := cursor.Err(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	getCategoriesResponse := GetCategoriesResponse{IsAuthorized: true, Categories: categories}

	//time.Sleep(2 * time.Second)
	json.NewEncoder(w).Encode(getCategoriesResponse)
}
