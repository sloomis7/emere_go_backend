package bookDC

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sloomis/emere_go_backend/cmd/app/models/book"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"go.mongodb.org/mongo-driver/bson"
)

//Get all books
func GetBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("books")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	cursor, err := collection.Find(ctx, bson.M{})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	var books []book.Book

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var book book.Book
		cursor.Decode(&book)
		books = append(books, book)
	}

	if err := cursor.Err(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	if books == nil {
		json.NewEncoder(w).Encode(&book.Book{})
	}

	json.NewEncoder(w).Encode(books)
}

//Create a New Book
func CreateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var book = book.CreateBook(r)

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("books")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	_, err := collection.InsertOne(ctx, book)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}
	json.NewEncoder(w).Encode(book)
}

//Get single book
func GetBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) //get Params

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("books")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	filter := bson.D{{"id", params["id"]}}

	// create a value into which the result can be decoded
	var book book.Book
	err := collection.FindOne(ctx, filter).Decode(&book)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(w).Encode(book)
}

//Update Book
func UpdateBook(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r) //get Params

	decoder := json.NewDecoder(r.Body)

	var book book.Book
	err := decoder.Decode(&book)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("books")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	filter := bson.D{{"id", params["id"]}}

	update := bson.D{{"$set",
		bson.D{
			{"isbn", book.Isbn},
			{"title", book.Title},
			{"author", book.Author},
		},
	}}
	// create a value into which the result can be decoded
	_, err = collection.UpdateOne(ctx, filter, update)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(w).Encode(book)
}

//Delete Book
func DeleteBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	params := mux.Vars(r) //get Params

	client := dbclientmanager.GetDBClient()
	collection := client.Database("EmereDB").Collection("books")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	filter := bson.D{{"id", params["id"]}}

	// create a value into which the result can be decoded
	var book book.Book
	err := collection.FindOne(ctx, filter).Decode(&book)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	_, err = collection.DeleteOne(ctx, bson.M{"id": params["id"]})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message: "` + err.Error() + `"}`))
		return
	}

	w.Write([]byte(`{"Delete Success: "` + params["id"] + `"}`))
}