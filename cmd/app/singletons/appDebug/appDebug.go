package appDebug

//AppDebug ..
type AppDebug struct {
	BypassAuthToken bool
	SimTokenExpired bool
}

var manager *AppDebug

func init() {

	manager = &AppDebug{}
	//manager.BypassAuthToken = true
	//manager.SimTokenExpired = true
}

//Shared - get singleton instance pre-initialized
func Shared() *AppDebug {
	return manager
}
