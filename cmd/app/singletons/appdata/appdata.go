package appdata

//Environment ..
type Environment string

const (
	prod Environment = "prod"
	dev              = "dev"
	test             = "test"
)

//AppData ..
type AppData struct {
	env                 Environment
	DevPort             string
	TokenExpirationTime int64
}

var manager *AppData

func init() {

	manager = &AppData{env: dev, DevPort: "9000"}
	manager.TokenExpirationTime = 30
}

//Shared - get singleton instance pre-initialized
func Shared() *AppData {
	return manager
}

//GetEnv ..
func GetEnv() Environment {
	return manager.env
}
