package router

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/bookDC"
	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/categoryDC"
	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/imageDC"
	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/productDC"
	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/userDC"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/appDebug"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"go.mongodb.org/mongo-driver/bson"
)

//routerManager ..
type routerManager struct {
	router *mux.Router
}

var manager *routerManager

func init() {

	router := mux.NewRouter()

	//Test Routes
	router.HandleFunc("/", test)
	//TODO: Test API - remove later
	router.HandleFunc("/api/books", bookDC.GetBooks).Methods("GET")
	router.HandleFunc("/api/books", bookDC.CreateBook).Methods("POST")
	router.HandleFunc("/api/books/{id}", bookDC.GetBook).Methods("GET")
	router.HandleFunc("/api/books/{id}", bookDC.UpdateBook).Methods("PUT")
	router.HandleFunc("/api/books/{id}", bookDC.DeleteBook).Methods("DELETE")

	router.HandleFunc("/api/signUp", userDC.SignUp).Methods("POST")
	router.HandleFunc("/api/signIn", userDC.SignIn).Methods("POST")
	router.HandleFunc("/api/login", isAuthorized(userDC.LoginWithToken)).Methods("GET")

	router.HandleFunc("/api/getCategories", isAuthorized(categoryDC.GetCategories)).Methods("GET")

	router.HandleFunc("/api/image/{folder}/{fileName}", imageDC.GetImage).Methods("GET")
	router.HandleFunc("/api/image/{folder}/{folder2}/{fileName}", imageDC.GetImage).Methods("GET")

	router.HandleFunc("/api/getProductDetails/{id}", isAuthorized(productDC.GetProductDetails)).Methods("GET")

	manager = &routerManager{router: router}

}

//AuthFailResponse ...
type AuthFailResponse struct {
	IsAuthorized bool `json:"isAuthorized"`
}

func isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) func(w http.ResponseWriter, r *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {

		//if test - bypass token check for postman
		if appDebug.Shared().BypassAuthToken {
			endpoint(w, r)
			return
		}

		loginFailResponse := AuthFailResponse{IsAuthorized: false}

		if appDebug.Shared().SimTokenExpired {
			//time.Sleep(3 * time.Second)

			json.NewEncoder(w).Encode(loginFailResponse)
			return
		}

		if r.Header["Token"] != nil {
			token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}

				return userDC.SigningKey, nil
			})

			if err != nil {
				//Token is not valid

				//Remove Token from DB
				//TODO: test
				sessionToken := r.Header["Token"][0]
				client := dbclientmanager.GetDBClient()
				collection := client.Database("EmereDB").Collection("sessions")
				ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

				filter := bson.D{{"sessiontoken", sessionToken}}

				// create a value into which the result can be decoded
				var session userDC.Session
				err := collection.FindOne(ctx, filter).Decode(&session)

				if err != nil {
					//Seesion not found
					fmt.Println(err.Error())
				}

				_, err = collection.DeleteOne(ctx, bson.M{"sessiontoken": sessionToken})
				if err != nil {
					//Couldn't Delete Session
					fmt.Println(err.Error())
				}
				//fmt.Println(err.Error())
				json.NewEncoder(w).Encode(loginFailResponse)
			}

			if token.Valid {
				endpoint(w, r)
			}

		} else {
			//No Token in Header
			fmt.Fprint(w, "No Token in Header")
		}
	}
}

//GetRouterManager - get singleton instance pre-initialized
func GetRouterManager() *routerManager {
	return manager
}

//GetRouter ..
func GetRouter() *mux.Router {
	return manager.router
}

func test(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`{"hello"}`))
}
