package dbclientmanager

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/sloomis/emere_go_backend/cmd/app/singletons/appdata"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

//dbClientManager ..
type dbClientManager struct {
	Client       *mongo.Client
	CloseContext context.Context
}

var manager *dbClientManager

func init() {
	//fmt.Println("init run")

	var client *mongo.Client

	//TODO: set to enum not string
	if appdata.GetEnv() == "prod" {
		client, _ = mongo.NewClient(options.Client().ApplyURI("mongodb+srv://scottloomis:broom1%40talks@cluster0.dhcn6.mongodb.net/EmereDB?retryWrites=true&w=majority"))

	} else {
		client, _ = mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err := client.Connect(ctx)

	if err != nil {
		fmt.Println("2")
		log.Fatal(err)
	}

	err = client.Ping(ctx, readpref.Primary())

	if err != nil {
		fmt.Println("3")
		log.Fatal(err)
	}

	databases, err := client.ListDatabaseNames(ctx, bson.M{})

	if err != nil {
		fmt.Println("4")
		log.Fatal(err)
	}
	fmt.Println(databases)

	manager = &dbClientManager{Client: client, CloseContext: ctx}
}

//GetDBClientManager - get singleton instance pre-initialized
func GetDBClientManager() *dbClientManager {
	return manager
}

//GetDBClient ..
func GetDBClient() *mongo.Client {
	return manager.Client
}
