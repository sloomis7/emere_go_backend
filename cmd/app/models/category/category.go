package category

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Category Model
type Category struct {
	ID         primitive.ObjectID `bson:"_id" json:"id"`
	Name       string             `json:"name"`
	Products   []Product          `json:"products"`
	IsFeatured bool               `json:"isFeatured"`
	ImageUrl   string             `json:"imageUrl"`
}

//Product Model
type Product struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Name     string             `json:"name"`
	Price    string             `json:"price"`
	ImageUrl string             `json:"imageUrl"`
}
