package user

import (
	"encoding/json"
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

//User Model
type User struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	FullName    string             `json:"fullName"`
	PhoneNumber string             `json:"phoneNumber"`
	Email       string             `json:"email"`
	Password    string             `json:"password"`
}

//ResponseUser Model
type ResponseUser struct {
	ID          string `json:"id"`
	FullName    string `json:"fullName"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"email"`
}

//SignInUser Model
type SignInUser struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func CreateUser(r *http.Request) User {

	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)

	return user
}
