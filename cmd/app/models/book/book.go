package book

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/sloomis/emere_go_backend/cmd/app/models/author"
)

//Book Model
type Book struct {
	ID     string         `json:"id"`
	Isbn   string         `json:"isbn"`
	Title  string         `json:"title"`
	Author *author.Author `json:"author"`
}

func CreateBook(r *http.Request) Book {

	var book Book
	_ = json.NewDecoder(r.Body).Decode(&book)

	//TODO: ID not random?
	book.ID = strconv.Itoa(rand.Intn(10000000))
	return book
}
