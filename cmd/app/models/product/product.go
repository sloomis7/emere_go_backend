package product

import (
	"math/rand"
	"time"

	"github.com/sloomis/emere_go_backend/cmd/app/models/category"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/appdata"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Product Model
type Product struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	Name        string             `json:"name"`
	Brand       string             `json:"brand"`
	Price       string             `json:"price"`
	PriceValue  float64            `json:"priceValue"`
	OldPrice    string             `json:"oldPrice"`
	ImageUrls   []string           `json:"imageUrls"`
	Description string             `json:"description"`
	//DescriptionExtra string             `json:"descriptionExtra"`
}

//Extra
//Review Rating

//CreateProductsData ...
func CreateProductsData() ([]category.Category, []Product) {

	rand.Seed(time.Now().UnixNano())

	var path string

	if appdata.GetEnv() == "prod" {
		path = "https://emere-go-backend.herokuapp.com/api/image/"
	} else {
		path = "http://10.0.0.196:" + appdata.Shared().DevPort + "/api/image/"
	}

	var categoryImagePath string = path + "categoryImages/"
	var productImagePath string = path + "productImages/"

	//Products

	productsArray := make([]Product, 0)
	newArrivalProductsArray := make([]category.Product, 0)
	featuredProductsArray := make([]category.Product, 0)
	bestSellersProductsArray := make([]category.Product, 0)

	tableLampsProductsArray := make([]category.Product, 0)
	officeChairsProductsArray := make([]category.Product, 0)
	sheetsProductsArray := make([]category.Product, 0)
	cookwareProductsArray := make([]category.Product, 0)
	juicersProductsArray := make([]category.Product, 0)
	coffeTablesProductsArray := make([]category.Product, 0)
	sofasProductsArray := make([]category.Product, 0)

	//Table Lamps

	tableLampPath := "tableLamps/"
	var newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "27.5\" Table Lamp"
	newProduct.Brand = "Joss & Main"
	newProduct.Price = "$85.99"
	newProduct.PriceValue = 85.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + tableLampPath + "1.jpg",
		productImagePath + tableLampPath + "1-2.jpg",
		productImagePath + tableLampPath + "1-3.jpg",
	}
	newProduct.Description = "Crafted from chic glass, this luminary provides ambient light to any space in need of a little sparkle! We especially love the crackle-like detail throughout the base, giving this piece the look of an antique the years of wear and tear! For a touch of on-trend contrast, this piece also features a chrome metal base and finial that ties the look together. An LED bulb is included, so you can start enjoying this lamp the moment it arrives!"

	productsArray = append(productsArray, newProduct)

	product := category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + tableLampPath + "1.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	tableLampsProductsArray = append(tableLampsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Kasen 19\" Table Lamp"
	newProduct.Brand = "Three Posts™"
	newProduct.Price = "$96.99"
	newProduct.PriceValue = 96.99
	newProduct.OldPrice = "$119.99"
	newProduct.ImageUrls = []string{
		productImagePath + tableLampPath + "2.jpg",
		productImagePath + tableLampPath + "2-2.jpg",
		productImagePath + tableLampPath + "2-3.jpg",
	}
	newProduct.Description = "This short table lamp is the ideal pick to create a contemporary glow in your living room or bedroom. It measures 18.5\" tall, making it ideal next to low-profile sofas. The glass urn-shaped base features metal trim accents and a central rod for a mixed-media appeal. Up top, there is a linen empire shade that diffuses the light from a 100W bulb (not included) inside. We love how this lamp has a 3-way rotary switch so you can have low or high levels of light."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + tableLampPath + "2.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	tableLampsProductsArray = append(tableLampsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Ashburnham 23.5\" Table Lamp"
	newProduct.Brand = "Greyleigh™"
	newProduct.Price = "$134.99"
	newProduct.PriceValue = 134.99
	newProduct.OldPrice = "$169.99"
	newProduct.ImageUrls = []string{
		productImagePath + tableLampPath + "3.jpg",
		productImagePath + tableLampPath + "3-2.jpg",
		productImagePath + tableLampPath + "3-3.jpg",
	}
	newProduct.Description = "This table lamp brings mixed-media style to your side table, console table, or mantel. The cylindrical base is made from concrete with an imprinted diamond design for some textural appeal. Three metallic bands accent the base and match the finial to give it a cohesive look. The linen drum shade caps the design and diffuses the light from a 100W bulb (not included) inside. We love how this lamp has a 3-way rotary switch so you can have low or high levels of light."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + tableLampPath + "3.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	tableLampsProductsArray = append(tableLampsProductsArray, product)

	//Office Chairs

	officeChairsPath := "officeChairs/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Mesh Task Chair"
	newProduct.Brand = "Symple Stuff"
	newProduct.Price = "$75.99"
	newProduct.PriceValue = 75.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + officeChairsPath + "2.jpg",
		productImagePath + officeChairsPath + "2-2.jpg",
		productImagePath + officeChairsPath + "2-3.jpg",
	}

	newProduct.Description = "This office chair offers extra comfortable support to the daily office needs. The soft and moderate sponge pad is suitable for your long sitting position. The backrest and armrests of the office chair are designed according to ergonomics so that even if you work for a long time, you will not feel tired. There is a tension adjustment knob at the bottom of and chairs, which can provide a certain inclination to relax you after work. The adjusting lever can help you adjust the height of the seat and make the seat reach your ideal height. Our office chair is suitable for all kinds of occasions. Besides office work, it is also fun to play video games while sitting on our chairs."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + officeChairsPath + "2.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	officeChairsProductsArray = append(officeChairsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Eiladh Home Office Computer Task Chair"
	newProduct.Brand = "Latitude Run®"
	newProduct.Price = "$89.99"
	newProduct.PriceValue = 89.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + officeChairsPath + "1.jpg",
		productImagePath + officeChairsPath + "1-2.jpg",
		productImagePath + officeChairsPath + "1-3.jpg",
	}
	newProduct.Description = "Our desk chair using a high-density sponge cushion, a more flexible, office chair with a middle back design, can provide good lumbar support. The computer chair uses a luxurious bonded leather back to make you feel comfortable. Equipped with 360-degree swivel wheels, running smoothly on floors. No matter you’re in the office, at your home, in a conference, or playing gaming, our mesh chair is a comfortable, stylish, and functional addition. It is a good choice to add one of our computer chairs in your office or put a desk chair in your home/activity room."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + officeChairsPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	officeChairsProductsArray = append(officeChairsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Office Task Chair"
	newProduct.Brand = "Inbox Zero"
	newProduct.Price = "$80.99"
	newProduct.PriceValue = 80.99
	newProduct.OldPrice = "$104.99"
	newProduct.ImageUrls = []string{
		productImagePath + officeChairsPath + "3.jpg",
		productImagePath + officeChairsPath + "3-2.jpg",
		productImagePath + officeChairsPath + "3-3.jpg",
	}
	newProduct.Description = "With all those hours spent working at a desk, getting an adjustable office chair that best suits your needs, ergonomics design, and the price is something that you should do for your health, this desk chair lends your home office a modern style!"

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + officeChairsPath + "3.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	officeChairsProductsArray = append(officeChairsProductsArray, product)

	//Sheets & Pillowcases

	sheetsPillowcasesPath := "sheetsPillowcases/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Sateen 300 Thread Count Cotton Blend Sheet Set"
	newProduct.Brand = "MOLECULE"
	newProduct.Price = "$97.30"
	newProduct.PriceValue = 97.30
	newProduct.OldPrice = "$139.00"
	newProduct.ImageUrls = []string{
		productImagePath + sheetsPillowcasesPath + "3.jpg",
		productImagePath + sheetsPillowcasesPath + "3-2.jpg",
		productImagePath + sheetsPillowcasesPath + "3-3.jpg",
	}

	newProduct.Description = "The Molecule Performance Sateen sheet set is luxuriously drapey and smooth. Sateen is a weave woven tightly, thus creating a heavier, perfect year-round sheet. It is comparable to the feel of silk, but with more durability leading to many years of comfort and better sleep. Its smooth, buttery feel is a constant reminder of luxury and comfort at its pinnacle. These sheets are OEKO-TEX certified and designed with advanced Tencel materials that keep you dry and cool, reduce bacterial growth, and help you discover healthier sleep."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sheetsPillowcasesPath + "3.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	sheetsProductsArray = append(sheetsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Garrin 1800 Thread Count Sheet Set"
	newProduct.Brand = "The Twillery Co.™"
	newProduct.Price = "$20.99"
	newProduct.PriceValue = 20.99
	newProduct.OldPrice = "$24.99"
	newProduct.ImageUrls = []string{
		productImagePath + sheetsPillowcasesPath + "1.jpg",
		productImagePath + sheetsPillowcasesPath + "1-2.jpg",
		productImagePath + sheetsPillowcasesPath + "1-3.jpg",
	}
	newProduct.Description = "An easy way to enhance the look of any master suite is to simply switch up the bedding! Crafted from 1800-thread count microfiber in a solid hue, these hypoallergenic sheets are a great option for drifting off to sleep with understated style. And since this set is wrinkle-free and safe for washing machines, a quick cycle is all it takes to lend it a fresh feeling."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sheetsPillowcasesPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	sheetsProductsArray = append(sheetsProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Everyday Sheet Set"
	newProduct.Brand = "Truly Soft"
	newProduct.Price = "$20.29"
	newProduct.PriceValue = 20.29
	newProduct.OldPrice = "$50.38"
	newProduct.ImageUrls = []string{
		productImagePath + sheetsPillowcasesPath + "2.jpg",
		productImagePath + sheetsPillowcasesPath + "2-2.jpg",
		productImagePath + sheetsPillowcasesPath + "2-3.jpg",
	}
	newProduct.Description = "Get warm and cozy with Truly Soft Everyday sheets. With revolutionary softness from finer yarns and soft finishes, you will not believe the difference our special techniques make until you touch these sheets. After a long day, you deserve a Truly Soft sheet for a good night’s rest, which is optimal for your health and well-being. Deep pocket sheets fit your mattress and stay tight with full elastic on all four sides. Coordinates with other Truly Soft items. The deep pocket flat and fitted sheet accommodates mattresses from 13\" to 15\" thick. Hypoallergenic, iron-safe, non-pilling, and wrinkle-resistant, this set can be machine washed and tumble dried in a gentle setting for fuss-free upkeep."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sheetsPillowcasesPath + "2.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	sheetsProductsArray = append(sheetsProductsArray, product)

	//Cookware Sets

	cookwareSetsPath := "cookwareSets/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Viking 11 Pieces Aluminum Cookware Set"
	newProduct.Brand = "Viking"
	newProduct.Price = "$379.99"
	newProduct.PriceValue = 379.99
	newProduct.OldPrice = "$1,000.00"
	newProduct.ImageUrls = []string{
		productImagePath + cookwareSetsPath + "4.jpg",
		productImagePath + cookwareSetsPath + "4-2.jpg",
		productImagePath + cookwareSetsPath + "4-3.jpg",
	}

	newProduct.Description = "Leverage the benefits of heat transfer that comes with cooking on aluminum with the non-reactive cooking surface of stainless steel with the Viking 2-Ply 11-Piece Cookware Set with Stainless Steel Lids. Available in an attractive color, this cookware set includes an exterior layer of heavy-duty aluminum for quick and even heating paired with a long-lasting, non-reactive stainless-steel interior cooking surface. The attractive exterior of the cookware set is comprised of a matte silicone polyester coating that is heat safe for high temperatures. Fully cast stay-cool stainless-steel handles are secured by stainless steel rivets for a lifetime of use. The Viking 2-Ply 11-Piece Cookware Set includes: an 8\"\" fry pan, a 10\"\" fry pan, a 2-quart saucepan with lid, a 3-quart saucepan with lid, a 5-quart Dutch oven with , a 3.5-quart sauté pan with lid and helper handle, and a steamer insert that fits the 3-quart saucepan. \nPerfect for gas, electric, ceramic and even induction (yes, it is induction compatible) cooktops, this cookware set from Viking will compliment any kitchen. This cookware set features a stainless steel induction plate embedded in the bottom of each pan to make it induction compatible, so whether you have induction now or you are considering it down the line, your cookware will be ready to make the transition with you whenever you are. Feel free to finish your dish in the oven or even cook directly in the oven. This set is oven safe to 450°. \nThe variety of sizes featured in the Viking 2-Ply Stainless Steel 11-Piece Cookware Set gives you the versatility you need to tackle a wide variety of dishes. From the perfect brunch, to playing dinner host, you can braise your coq au vin in the Dutch oven while sautéing up the perfect side dish in your fry pan! \nCaring for your cookware set is easy. This Viking 11-pc cookware set is dishwasher safe. We always recommend extending the life of your cookware and to avoid any rust spots around the induction plate by hand washing with warm, soapy water and either let air-dry or wipe with a dishtowel before storing. Limited lifetime warranty."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + cookwareSetsPath + "4.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	cookwareProductsArray = append(cookwareProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Kenmore 12 Pieces Aluminum Non Stick Cookware Set"
	newProduct.Brand = "Kenmore"
	newProduct.Price = "$125.32"
	newProduct.PriceValue = 125.32
	newProduct.OldPrice = "$139.99"
	newProduct.ImageUrls = []string{
		productImagePath + cookwareSetsPath + "1.jpg",
		productImagePath + cookwareSetsPath + "1-2.jpg",
		productImagePath + cookwareSetsPath + "1-3.jpg",
	}
	newProduct.Description = "The Kenmore 12-Piece Nonstick Ceramic Cookware set includes all the essential pots, pans, and cooking utensils you need to do amazing things in the kitchen. Sleek and stackable, you’ll find yourself reaching for one of these convenient pieces. The vibrant metallic enamel exterior and non-stick interior surfaces mean each piece is easy to clean and will also add a pop of color to your kitchen. Crafted from forged aluminum with riveted bakelite handles this cookware heats up quickly and evenly— and Kenmore quality means they’ll last a lifetime. Whether you’re sautéing veggies, boiling pasta, frying fish, or serving an oven-to-table meal in your Dutch oven, these non-stick pots and pans lock in flavor while allowing easy food release. Perfect for veteran chefs who require effortless performance or others simply looking to expand their culinary palette, the Kenmore 12-Piece Nonstick Ceramic Interior Cookware Set delivers all the essentials for cooking delicious meals at home. Enjoy a toxic-free and healthy lifestyle through the high-quality ceramic coating on all these sets making the Kenmore Cookware completely PFOA and PTFE-free."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + cookwareSetsPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	cookwareProductsArray = append(cookwareProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Rachael Ray 10 Piece Hard-Anodized Aluminum Non Stick Cookware Set"
	newProduct.Brand = "Rachael Ray"
	newProduct.Price = "$149.46"
	newProduct.PriceValue = 149.46
	newProduct.OldPrice = "$339.99"
	newProduct.ImageUrls = []string{
		productImagePath + cookwareSetsPath + "2.jpg",
		productImagePath + cookwareSetsPath + "2-2.jpg",
		productImagePath + cookwareSetsPath + "2-3.jpg",
	}
	newProduct.Description = "With bold pops of color and durable non-stick surfaces, the Rachael Ray 10 Piece Hard-Anodized Aluminum Non-Stick Cookware Set makes meal prep hassle-free and fun. The non-stick cookware set features hard-anodized construction that heats swiftly and evenly, and the pot and pan interiors are coated in durable non-stick for impeccable food release. Grippy handles on the cookware are rubberized for comfort and double riveted for strength, and shatter-resistant glass lids make it easy to monitor foods while they cook without losing heat or moisture. This cooking set is oven safe to 350°F and dishwasher safe for convenience, plus the pots and pans complement many other pieces in Rachael's collections. Get convenience and performance in full color with the Rachael Ray 10 Piece Hard-Anodized Aluminum Non-Stick Cookware Set."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + cookwareSetsPath + "2.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	cookwareProductsArray = append(cookwareProductsArray, product)

	//Juicers

	juicersPath := "juicers/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Black + Decker Citrus Juicer"
	newProduct.Brand = "Black + Decker"
	newProduct.Price = "$20.14"
	newProduct.PriceValue = 20.14
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + juicersPath + "5.jpg",
		productImagePath + juicersPath + "5-2.jpg",
		productImagePath + juicersPath + "5-3.jpg",
	}

	newProduct.Description = "Freshly squeezed juice is just a spin away, this juicer features a dual-purpose, self-reversing cone that ensures maximum juice extraction from large and small citrus. Juice flows into the easy-pour pitcher, which includes measurement markings for accurate use in recipes and mixed drinks. Plus, all removable parts are dishwasher safe for easy cleanup."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + juicersPath + "5.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	juicersProductsArray = append(juicersProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Green4ever Power Centrifugal Juicer"
	newProduct.Brand = "Green4ever"
	newProduct.Price = "$59.06"
	newProduct.PriceValue = 59.06
	newProduct.OldPrice = "$69.99"
	newProduct.ImageUrls = []string{
		productImagePath + juicersPath + "1.jpg",
		productImagePath + juicersPath + "1-2.jpg",
		productImagePath + juicersPath + "1-3.jpg",
	}
	newProduct.Description = "The power centrifugal juicer extractor machine with led lights is suitable for making fruit and vegetable juices, supplementing vitamins anytime and anywhere to satisfy all your preferences. It is a good and indispensable tool in your home, starting your great day with a great cup of juice."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + juicersPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	juicersProductsArray = append(juicersProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Weston Super Chute Juicer"
	newProduct.Brand = "Weston"
	newProduct.Price = "$149.99"
	newProduct.PriceValue = 149.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + juicersPath + "2.jpg",
		productImagePath + juicersPath + "2-2.jpg",
		productImagePath + juicersPath + "2-3.jpg",
	}
	newProduct.Description = "Super Chute Juicer lets you create your own fresh juice blends at home. Juicing is quick and easy with the extra-large 3.5\" chute. Two-speed controls allow for custom juicing and maximum juice extraction. Choose high speed for harder vegetables and fruits, and low speed for soft fruits and leafy greens. Juicing fruits and vegetables into nutritious and delicious drinks makes getting your daily servings easy. The juicer fits under most standard cabinets so you can have it at the ready for juicing anytime. Cleanup is fast, the juicer disassembles quickly and most parts just need a quick rinse. A large pulp bin lets you juice more while emptying the bin less."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + juicersPath + "2.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	juicersProductsArray = append(juicersProductsArray, product)

	//Coffee Tables

	coffeeTablesPath := "coffeeTables/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Fuentes Coffee Table with Storage"
	newProduct.Brand = "Union Rustic"
	newProduct.Price = "$179.99"
	newProduct.PriceValue = 179.99
	newProduct.OldPrice = "$329.99"
	newProduct.ImageUrls = []string{
		productImagePath + coffeeTablesPath + "6.jpg",
		productImagePath + coffeeTablesPath + "6-2.jpg",
		productImagePath + coffeeTablesPath + "6-3.jpg",
	}

	newProduct.Description = "Upgrade your living room with this coffee table. The spacious tabletop and lower shelf provide plenty of space to organize remotes, books, magazines, and tablets. The lower shelf keeps the tabletop clutter-free and keeps essentials within arm's reach. The laminated hollow core tabletop and shelf are accented by the powder-coated metal legs for a stylish design and durable build. The timeless finishes pair with black metal legs for a mix of classic and industrial styles. The coffee table ships flat to your door and requires assembly upon opening. Two adults are recommended to assemble."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + coffeeTablesPath + "6.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	coffeTablesProductsArray = append(coffeTablesProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Coffee Table"
	newProduct.Brand = "Latitude Run®"
	newProduct.Price = "$139.99"
	newProduct.PriceValue = 139.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + coffeeTablesPath + "1.jpg",
		productImagePath + coffeeTablesPath + "1-2.jpg",
		productImagePath + coffeeTablesPath + "1-3.jpg",
	}
	newProduct.Description = "Our glass coffee table will be one of your ideal modern furniture choices. The minimalist style design can be perfectly integrated into your modern furniture atmosphere without any abrupt feeling. The design of the double-layer space structure allows you to have sufficient storage space to place your favorite teacups, books, magazines, etc. 8mm thick tempered glass with solid metal legs make our coffee table more reliable and durable than other wooden leg coffee tables. What makes you even more incredible is that, referring to our attached installation manual, the installation of our coffee table can be completed in a few steps.Such a simple installation design is just to bring you a better product experience. Please do not miss our product which is absolute excellent quality and reasonable price!"

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + coffeeTablesPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	coffeTablesProductsArray = append(coffeTablesProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Isakson Trestle Coffee Table"
	newProduct.Brand = "Laurel Foundry Modern Farmhouse®"
	newProduct.Price = "$259.99"
	newProduct.PriceValue = 259.99
	newProduct.OldPrice = "$344.99"
	newProduct.ImageUrls = []string{
		productImagePath + coffeeTablesPath + "2.jpg",
		productImagePath + coffeeTablesPath + "2-2.jpg",
		productImagePath + coffeeTablesPath + "2-3.jpg",
	}
	newProduct.Description = "Center your living room or den around charming style with this lovely coffee table. The perfect pick for modern farmhouse aesthetics, it showcases a streamlined frame with a rustic wood top supported by two x-crossed sides. The sides are crafted from metal and finished in a black hue for a touch of industrial appeal, while the lower shelf provides a convenient space to tuck away glossy magazines, media accessories, and more."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + coffeeTablesPath + "2.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	coffeTablesProductsArray = append(coffeTablesProductsArray, product)

	//Sofas

	sofasPath := "sofas/"
	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Celestia Microfiber / Microsuede 56.3\" Flared Arms Loveseat"
	newProduct.Brand = "Andover Mills™"
	newProduct.Price = "$295.99"
	newProduct.PriceValue = 295.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + sofasPath + "7.jpg",
		productImagePath + sofasPath + "7-2.jpg",
		productImagePath + sofasPath + "7-3.jpg",
	}

	newProduct.Description = "The Celestia collection sofa is a lovely curved arm stationary sofa that offers the perfect place for family, friends, or casual acquaintances to rest, relax, and recharge. The wooden frame offers only the finest protection from all manner of bouncing or jostling that furniture unfortunately gets. The premium Dark Gray or Coffee microfiber fabric upholstery provides ultimate comfort and style to your living space. Minimal assembly is required, but this is only a minor inconvenience to ensure the best outcome – both while serving as decoration and in use. So bring some class back into your sitting room – choose the Celestia set for your home!"

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sofasPath + "7.jpg",
	}
	newArrivalProductsArray = append(newArrivalProductsArray, product)
	sofasProductsArray = append(sofasProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Ibiza 80.3\" Flared Arm Sofa"
	newProduct.Brand = "Zipcode Design™"
	newProduct.Price = "$353.99"
	newProduct.PriceValue = 353.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + sofasPath + "1.jpg",
		productImagePath + sofasPath + "1-2.jpg",
		productImagePath + sofasPath + "1-3.jpg",
	}
	newProduct.Description = "The Ibiza Sofa is the three seat option of this versatile stationary set designed to fit your space and style. Built for lasting quality, the Ibiza combines form, function and ease of assembly designed for the eclectic life. The design lines and scale of the Ibiza Sofa brings a modern touch to spaces large and small with equal elegance. High density foam cushions and curved arms hug your body and is sure to become your favorite place to let go of the day and relax in pure comfort. Combining premium upholstered fabric and solid eucalyptus wooden frame, this durable plush Sofa invites you to rest in the knowledge of years of enjoyment to come."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sofasPath + "1.jpg",
	}
	featuredProductsArray = append(featuredProductsArray, product)
	sofasProductsArray = append(sofasProductsArray, product)

	newProduct = Product{}
	newProduct.ID = primitive.NewObjectID()
	newProduct.Name = "Niemeyer 51.5\" Tuxedo Arms Loveseat"
	newProduct.Brand = "Hashtag Home"
	newProduct.Price = "$309.99"
	newProduct.PriceValue = 309.99
	newProduct.OldPrice = ""
	newProduct.ImageUrls = []string{
		productImagePath + sofasPath + "2.jpg",
		productImagePath + sofasPath + "2-2.jpg",
		productImagePath + sofasPath + "2-3.jpg",
	}
	newProduct.Description = "This clean-lined loveseat proves you don't have to skimp on style if you're short on square footage. Its mid-century modern design is crafted on top of splayed wooden legs in a natural stain for a warm and Scandinavian-inspired touch. A foam fill and polyester fabric upholstery cushions the wooden frame for a more approachable look. The subtle button-tufted details dot the back cushions for a dash of distinction. To assemble, just attach this loveseat’s legs. And to clean, we recommend wiping with a cloth and mild soap."

	productsArray = append(productsArray, newProduct)

	product = category.Product{
		ID:       newProduct.ID,
		Name:     newProduct.Name,
		Price:    newProduct.Price,
		ImageUrl: productImagePath + sofasPath + "2.jpg",
	}
	bestSellersProductsArray = append(bestSellersProductsArray, product)
	sofasProductsArray = append(sofasProductsArray, product)

	//Categories

	categoriesArray := make([]category.Category, 0)

	newCategory := category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "New Arrivals"
	newCategory.IsFeatured = true
	newCategory.ImageUrl = ""
	newCategory.Products = newArrivalProductsArray
	rand.Shuffle(len(newCategory.Products), func(i, j int) {
		newCategory.Products[i], newCategory.Products[j] = newCategory.Products[j], newCategory.Products[i]
	})

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Featured"
	newCategory.IsFeatured = true
	newCategory.ImageUrl = ""
	newCategory.Products = featuredProductsArray
	rand.Shuffle(len(newCategory.Products), func(i, j int) {
		newCategory.Products[i], newCategory.Products[j] = newCategory.Products[j], newCategory.Products[i]
	})

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Best Sellers"
	newCategory.IsFeatured = true
	newCategory.ImageUrl = ""
	newCategory.Products = bestSellersProductsArray
	rand.Shuffle(len(newCategory.Products), func(i, j int) {
		newCategory.Products[i], newCategory.Products[j] = newCategory.Products[j], newCategory.Products[i]
	})

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Table Lamps"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "1.jpg"
	newCategory.Products = tableLampsProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Office Chairs"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "2.jpg"
	newCategory.Products = officeChairsProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Sheets & Pillowcases"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "3.jpg"
	newCategory.Products = sheetsProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Cookware Sets"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "4.jpg"
	newCategory.Products = cookwareProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Juicers"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "5.jpg"
	newCategory.Products = juicersProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Coffee Tables"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "6.jpg"
	newCategory.Products = coffeTablesProductsArray

	categoriesArray = append(categoriesArray, newCategory)

	newCategory = category.Category{}
	newCategory.ID = primitive.NewObjectID()
	newCategory.Name = "Sofas"
	newCategory.Products = make([]category.Product, 0)
	newCategory.ImageUrl = categoryImagePath + "7.jpg"
	newCategory.Products = sofasProductsArray

	categoriesArray = append(categoriesArray, newCategory)
	rand.Shuffle(len(categoriesArray), func(i, j int) { categoriesArray[i], categoriesArray[j] = categoriesArray[j], categoriesArray[i] })

	return categoriesArray, productsArray
}
