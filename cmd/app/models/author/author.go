package author

type Author struct {
	Firstname string `json:"firstName"`
	Lastname  string `json:"lastname"`
}