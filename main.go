package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/sloomis/emere_go_backend/cmd/app/dataControllers/productDC"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/appdata"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/dbclientmanager"
	"github.com/sloomis/emere_go_backend/cmd/app/singletons/router"
)

func main() {

	clientManager := dbclientmanager.GetDBClientManager()
	defer clientManager.Client.Disconnect(clientManager.CloseContext)

	port := os.Getenv("PORT")

	if port == "" {
		port = appdata.Shared().DevPort
	}

	if appdata.GetEnv() == "test" {

		productDC.RemoveProductsAndCategories()

	} else {

		//productDC.RemoveProductsAndCategories()
		productDC.CreateProductsTest()

		fmt.Println("Listening on: " + port)

		//cert, _ := tls.LoadX509KeyPair("")

		//TODO: Fix this later for env
		log.Fatal(http.ListenAndServe(":"+port, router.GetRouter()))
		//log.Fatal(http.ListenAndServeTLS(":"+port, "server.crt", "server.key", routermanger.GetRouter()))

		// s := &http.Server{
		// 	Addr:    ":" + port,
		// 	Handler: router.GetRouter(),
		// }

		// log.Fatal(s.ListenAndServeTLS("server.crt", "server.key"))
	}

}
